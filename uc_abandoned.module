<?php
/**
 * @file
 * Shared code for the abandoned cart and order sub-modules.
 */

/**
 * Implements hook_menu().
 */
function uc_abandoned_menu() {
  $items = array();

  $items['uc_abandoned/cart_redirect'] = array(
    'title' => 'Login cart redirect',
    'page callback' => 'uc_abandoned_cart_redirect',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Implements hook_theme().
 */
function uc_abandoned_theme($existing, $type, $theme, $path) {
  return array(
    'uc_abandoned_notification' => array(
      'template' => 'uc-abandoned-notification',
      'path' => $path,
      'variables' => array(
        'products' => NULL,
        'thank_you_message' => TRUE,
        'help_text' => TRUE,
        'email_text' => TRUE,
        'store_footer' => TRUE,
        'business_header' => TRUE,
        'shipping_method' => TRUE,
      ),
    ),
  );
}

/**
 * Implements hook_mail().
 */
function uc_abandoned_mail($key, &$message, $params) {
  $langcode = isset($message['language']) ? $message['language']->language : NULL;
  // Build the appropriate message paramaters based on the e-mail key.
  switch ($key) {
    case 'action-mail':
      // Assemble an email message from the conditional actions settings.

      //$message['headers']['MIME-Version'] = '1.0';
      //$message['headers']['X-Mailer'] = 'Drupal';
      $message['headers']['Content-Transfer-Encoding'] = '8Bit';

      $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; format=flowed';

      $message['from'] = $params['from'];

      // Perform token replacement on the subject and body.
      $subject = token_replace($params['subject'], $params['replacements'], $langcode ? array('language' => $message['language']) : array());
      $body = token_replace($params['message'], $params['replacements'], $langcode ? array('language' => $message['language']) : array());
      // Strip newline characters from e-mail subjects.
      // @todo: Maybe drupal_mail_send() should do this?
      $message['subject'] = str_replace(array("\r\n", "\r", "\n"), ' ', $subject);

      // Apply an input format to the message body if specified.
      //$params['format'] = 'full_html';
      if (isset($params['format'])) {
        $message['body'] = explode("\n", check_markup($body, $params['format'], $langcode));
      }
      else {
        $message['body'] = explode("\n", $body);
      }
      break;
  }
}

/**
 * Wrapper function to return if the user has opted in/out of receiving
 * notifications.
 */
function uc_abandoned_can_send_mail($account) {
  if ($field_name = variable_get('uc_abandoned_notification_preference_field', NULL)) {
    if ($items = field_get_items('user', $account, $field_name)) {
      return $items[0]['value'] == 1;
    }
  }

  return TRUE;
}

/**
 * Returns a common structure from either the order products or the cart.
 */
function uc_abandoned_get_products($mixed) {
  $products = array();
  if (count($mixed) && isset(reset($mixed)->order_product_id)) {
    // Order
    $display = entity_view('uc_order_product', $mixed);
    foreach ($mixed as $order_product_id => $order_product) {

      $details = '';
      if (!empty($order_product->data['attributes'])) {
        $attributes = array();
        foreach ($order_product->data['attributes'] as $attribute => $option) {
          $attributes[] = t('@attribute: @options', array('@attribute' => $attribute, '@options' => implode(', ', (array)$option)));
        }
        $details .= theme('item_list', array('items' => $attributes));
      }

      $product = (object) array(
        'title' => $order_product->title,
        'model' => $order_product->model,
        'qty' => $order_product->qty,
        'price' => $order_product->price,
        'total_price' => render($display['uc_order_product'][$order_product_id]['total']),
        'details' => $details,
      );

      if ($order_product->qty > 1) {
        $product->individual_price = t('(!price each)', array('!price' => uc_currency_format($display['uc_order_product'][$order_product_id]['price']['#price'])));
      }
      else {
        $product->individual_price = '';
      }

      $products[] = $product;
    }
  }
  else {
    // Cart
    $display_items = entity_view('uc_cart_item', $mixed, 'cart');
    foreach ($mixed as $cart_item_id => $cart_item) {
      $display_item = $display_items['uc_cart_item'][$cart_item_id];

      $price = array(
        '#theme' => 'uc_price',
        '#price' => $display_item['#total'],
      );
      if (!empty($display_item['#suffixes'])) {
        $total_price['#suffixes'] = $display_item['#suffixes'];
      }

      $product = (object) array(
        'title' => $cart_item->title,
        'model' => $cart_item->model,
        'qty' => $cart_item->qty,
        'price' => $cart_item->sell_price,
        'total_price' => render($price),
        'details' => $display_item['description']['#markup'],
      );

      if ($product->qty > 1) {
        $product->individual_price = t('(!price each)', array('!price' => uc_currency_format($cart_item->sell_price)));
      }
      else {
        $product->individual_price = '';
      }

      $products[] = $product;
    }
  }

  return serialize($products);
}

/**
 * Preprocesses a formatted notification for abandoned shopping.
 *
 * @see uc-abandoned-notification.tpl.php
 */
function template_preprocess_uc_abandoned_notification(&$variables) {
  // Generate tokens to use as template variables.
  $types = array(
  );

  $token_info = token_info();

  $replacements = array();
  foreach (array('site', 'store') as $type) {
    $replacements[$type] = token_generate($type, drupal_map_assoc(array_keys($token_info['tokens'][$type])), $types);
  }

  foreach ($replacements as $type => $tokens) {
    foreach ($tokens as $token => $value) {
      $key = str_replace('-', '_', $type . '_' . $token);
      $key = str_replace('uc_', '', $key);
      $variables[$key] = $value;
    }
  }
}

/**
 * Menu callback that redirects users to the cart or to login, then the cart.
 */
function uc_abandoned_cart_redirect() {
  global $user;
  if ($user->uid) {
    $query = drupal_get_query_parameters();
    drupal_goto('cart', array('query' => $query));
  }
  else {
    $query = drupal_get_query_parameters();
    $query['destination'] = 'cart';
    drupal_goto('user', array('query' => $query));
  }
}
