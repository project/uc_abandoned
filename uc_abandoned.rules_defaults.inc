<?php
/**
 * @file
 * This file contains the Rules hooks and functions necessary to send
 * email reminders for abandoned shopping.
 */

function uc_abandoned_default_rules_configuration() {
  $configs = array();

  $rule = '{ "uc_abandoned_email_notification" : {
      "LABEL" : "Ubercart abandoned shopping reminder notification",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules", "uc_abandoned" ],
      "USES VARIABLES" : {
        "mail" : { "label" : "E-mail", "type" : "text" },
        "products" : { "label" : "Products", "type" : "text" }
      },
      "ACTION SET" : [
        { "uc_abandoned_email_notification" : {
            "products" : [ "products" ],
            "addresses" : [ "mail" ],
            "subject" : "Reminder to complete your checkout at [store:name]"
          }
        }
      ]
    }
  }';
  $configs['uc_abandoned_email_notification'] = rules_import($rule);
  return $configs;
}

