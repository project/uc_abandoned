<?php
/**
 * @file
 * This file is the default abandoned cart template for Ubercart.
 *
 * Available variables:
 * - $products: An array of product objects in the order, with the following
 *   members:
 *   - title: The product title.
 *   - model: The product SKU.
 *   - qty: The quantity ordered.
 *   - total_price: The formatted total price for the quantity ordered.
 *   - individual_price: If quantity is more than 1, the formatted product
 *     price of a single item.
 *   - details: Any extra details about the product, such as attributes.
 *
 * Tokens: All site, store and order tokens are also available as
 * variables, such as $site_logo, $store_name and $order_first_name.
 *
 * Display options:
 * - $business_header: TRUE if the invoice header should be displayed.
 * - $shipping_method: TRUE if shipping information should be displayed.
 * - $help_text: TRUE if the store help message should be displayed.
 * - $email_text: TRUE if the "do not reply to this email" message should
 *   be displayed.
 * - $store_footer: TRUE if the store URL should be displayed.
 * - $thank_you_message: TRUE if the 'thank you for your order' message
 *   should be displayed.
 *
 * @see template_preprocess_uc_order()
 */
?>
<style type="text/css">
  a.hoverlink { text-decoration: none; }
  a.hoverlink:hover { text-decoration: underline; }
</style>
<table width="95%" border="0" cellspacing="0" cellpadding="1" align="center" bgcolor="#006699" style="font-family: verdana, arial, helvetica; font-size: small;">
  <tr>
    <td>
      <table width="100%" border="0" cellspacing="0" cellpadding="5" align="center" bgcolor="#FFFFFF" style="font-family: verdana, arial, helvetica; font-size: small;">
        <?php if ($business_header): ?>
        <tr valign="top">
          <td>
            <table width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
              <tr>
                <td>
                  <a href="#"><img src="<?php print base_path(); ?>sites/default/files/images/logo.png" border="0" alt="" /></a>
                </td>
                <td width="98%">
                  <div style="padding-left: 1em;">
                  <span style="font-size: large;"><?php print $store_name; ?></span><br />
                  <?php print $site_slogan; ?>
                  </div>
                </td>
                <td nowrap="nowrap">
                  &nbsp;
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <?php endif; ?>

        <tr valign="top">
          <td>

            <p><?php echo t('We noticed that you left !store_name before finishing your order.', array('!store_name' => $store_name)); ?><br />
            <?php print t('What prevented you from completing the order?'); ?></p>

            <p><?php print t('Please feel free to reach out to us at !store_phone if you have any questions, or you can click the link below to complete your order.', array('!store_phone' => $store_phone)); ?></p>

            <table cellpadding="4" cellspacing="0" border="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
              <tr>
                <td colspan="2" bgcolor="#006699" style="color: white;">
                  <b><?php print t('Cart Summary:'); ?></b>
                </td>
              </tr>

              <tr>
                <td colspan="2">

                  <table border="0" cellpadding="1" cellspacing="0" width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
                    <!--
                    <tr>
                      <td nowrap="nowrap">
                        <?php print t('Cart reference #:'); ?>
                      </td>
                      <td width="98%">
                        <?php print $order_order_id; ?>
                      </td>
                    </tr>

                    <tr>
                      <td height="20" colspan="2">&nbsp;</td>
                    </tr>
                    -->

                    <tr>
                      <td colspan="2">
                        <?php print t('Your cart contained the following products:'); ?>

                        <table width="100%" style="font-family: verdana, arial, helvetica; font-size: small;">
                          <?php foreach ($products as $product): ?>
                          <tr>
                            <td valign="top" nowrap="nowrap">
                              <b><?php print $product->qty; ?> x </b>
                            </td>
                            <td width="98%">
                              <b><?php print $product->title; ?> - <?php print $product->total_price; ?></b>
                              <?php print $product->individual_price; ?><br />
                              <?php print t('SKU'); ?>: <?php print $product->model; ?><br />
                              <?php print $product->details; ?>
                            </td>
                          </tr>
                          <?php endforeach; ?>
                        </table>
                      </td>
                    </tr>
                  </table>

                </td>
              </tr>

              <?php if ($help_text || $email_text || $store_footer): ?>
              <tr>
                <td colspan="2">
                  <hr noshade="noshade" size="1" /><br />

                  <?php
                  print l(t('Visit your cart'), 'uc_abandoned/cart_redirect', array(
                    'attributes' => array('class' => 'hoverlink', 'style' => 'border: 2px solid #cb2626; margin-bottom: 1em; padding: 0.8em 3em; color: #cb2626; font-size: 1.3em;display: inline-block; text-align: center;'),
                  ));
                  ?>

                  <p><?php print t('Thank you for shopping with us.'); ?></p>

                  <?php if ($store_footer): ?>
                  <p><b><?php print $store_link; ?></b><br /><b><?php print $site_slogan; ?></b></p>
                  <?php endif; ?>
                </td>
              </tr>
              <?php endif; ?>

            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

