<?php
/**
 * @file
 * Rules integration for uc_abandoned.module.
 */

/**
 * Implements hook_rules_action_info().
 */
function uc_abandoned_rules_action_info() {
  $action = array();

  $actions['uc_abandoned_email_notification'] = array(
    'label' => t('Email an abandoned shopping notification'),
    'group' => t('Ubercart Abandoned'),
    'base' => 'uc_abandoned_action_email_notification',
    'parameter' => array(
      'products' => array(
        'type' => 'text',
        'label' => t('Products'),
      ),
      'from' => array(
        'type' => 'text',
        'label' => t('Sender'),
        'description' => t("Enter the 'From' email address, or leave blank to use your store email address. You may use order tokens for dynamic email addresses."),
        'optional' => TRUE,
      ),
      'addresses' => array(
        'type' => 'text',
        'label' => t('Recipients'),
        'description' => t('Enter the email addresses to receive the invoice, one on each line. You may use order tokens for dynamic email addresses.'),
      ),
      'subject' => array(
        'type' => 'text',
        'label' => t('Subject'),
        'translatable' => TRUE,
      ),
    ),
  );

  return $actions;
}

/**
 * Emails an abandoned cart or order notification.
 *
 * The 'Sender', 'Subject' and 'Addresses' fields take order token replacements.
 */
function uc_abandoned_action_email_notification($products, $from, $addresses, $subject) {
  $products = unserialize($products);

  $settings = array(
    'from' => $from,
    'addresses' => $addresses,
    'subject' => $subject,
    'replacements' => array(),
  );

  // Split up our recipient e-mail addresses.
  $recipients = array();
  foreach (explode("\n", $addresses) as $address) {
    $address = trim($address);
    // Remove blank lines
    if (!empty($address)) {
      $recipients[] = $address;
    }
  }

  $settings['message'] = theme('uc_abandoned_notification', array('products' => $products));

  foreach ($recipients as $email) {
    $sent = drupal_mail('uc_abandoned', 'action-mail', $email, uc_store_mail_recipient_language($email), $settings, $from);

    if (!$sent['result']) {
      watchdog('uc_abandoned', 'Attempt to e-mail abandoned shopping notification to @email failed.', array('@email' => $email), WATCHDOG_ERROR);
    }
  }
}
