What does Ubercart Abandoned Cart and Order Reminders do?
---------------------------------------------------------
This module allows you to send reminder e-mail notifications to customers who
have abandoned a cart or order on your site. The notification template can be
customized to your needs, and you may choose to enable notifications for
abandoned carts and abandoned order independently.


Requirements
------------
* PHP 5
* Rules
* Ubercart's cart (uc_cart) and optionally order (uc_order) modules


Which modules do I enable?
--------------------------
* uc_abandoned provides a basic framework for sending reminder notifications and
  Rules integration. This module is required if you wish to use the module.

* uc_abandoned_cart provides support for detecting inactivity in authenticated
  users' shopping cards. After a configurable delay without any activity, a
  shopping cart will be considered abandoned and a notification will be queued.
  At this time, uc_abandoned_cart has no support for anonymous user as there is
  no way to collect their e-mail.

* uc_abandoned_order provides support for detecting abandoned orders based on
  Ubercart's 'Abandoned' order status. The users associated to orders marked as
  such will be sent a reminder notification. Both anonymous and authenticated
  users are supported, provided anonymous users have passed the point in
  checkout where they enter their e-mail.


Configuration
-------------
Be sure to have your site configured to run Cron regularly; abandoned
notifications are only sent when cron runs.

Both the cart and order modules have a configurable 'reachback' time which
determines how far back in time the module check for abandoned carts or orders.
The default reachback time is 172800 seconds (48 hours), but you override this
by setting the following variables:
* uc_abandoned_order_reachback for abandoned order detection
* uc_abandoned_cart_reachback for abandoned cart detection

As well, note that the inactivity delay cutoff at which point a shopping cart is
considered abandoned is also configurable and defaults to 7200 seconds. It can
be overriden by setting the variable uc_abandoned_cart_inactivity_threshold.

Lastly, if a Boolean field has been added to user profiles on the site it can
be linked to uc_abandoned so that when the field has a value of FALSE on the
user's profile, notification mails will not be sent to the user. Simply set the
uc_abandoned_notification_preference_field variable to the name of the field on
user accounts.


Customizing the reminder notification e-mail
--------------------------------------------
The default abandoned notification e-mail template is bundled in the module as
uc-abandoned-notification.tpl.php. You may customize it as you see fit by
copying this file to your theme folder and editing its contents. Note that you=
may also want to copy or link the file in your admin theme if cron is sometimes
triggered from the administration side.


Known issues and limitations
----------------------------
* At the moment, the module has no administration interface and is intended for
  use by webmasters. It is most easily configured using Drush.

* The module has been designed for a workflow where users must login to
  checkout. Disabling anonymous checkout is not required but highly recommended.

* This module may or may not comply with local e-mail spam laws; the module
  developers are not responsible for this. Please verify and adjust the reminder
  notification e-mail content as necessary.


Credits and Sponsors
--------------------
Development of this module was performed by Grindflow Management LLC,
http://www.grindflow.com.

This module was generously sponsored by Serialio.com, your mobile solution
provider on thousands of device models.
