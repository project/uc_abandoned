<?php
/**
 * @file
 * Periodically checks for abandoned carts and emails reminds to users with
 * links to complete their checkout.
 */

/**
 * Implements hook_cron().
 */
function uc_abandoned_cart_cron() {
  // First notification; set timestamp for next reminder if necessary
  $cart_ids = uc_abandoned_cart_get_carts();
  foreach ($cart_ids as $cart_id) {
    // Cart IDs are also user IDs if they aren't an anonymous token (length is
    // less than 22).
    if ($account = user_load($cart_id)) {
      if (!uc_abandoned_can_send_mail($account)) {
        continue;
      }
      $cart_products = uc_cart_get_contents($cart_id);

      db_insert('uc_abandoned_notifications')
        ->fields(array(
          'primary_email' => $account->mail,
          'notified' => REQUEST_TIME,
          'next_notification' => NULL,
          'type' => 'cart',
          'foreign_id' => $cart_id,
      ))->execute();
      rules_invoke_component('uc_abandoned_email_notification', $account->mail, uc_abandoned_get_products($cart_products));
    }
  }
}

/**
 * Retrieves a list of users who have abandoned orders and who have not yet
 * received a notification.
 */
function uc_abandoned_cart_get_carts() {
  // How far back in orders we'll look
  $cart_cutoff_low = REQUEST_TIME - variable_get('uc_abandoned_cart_reachback', 172800);
  $cart_cutoff_high = REQUEST_TIME - variable_get('uc_abandoned_cart_inactivity_threshold', 7200);

  $cart_ids = db_query("SELECT cp.cart_id FROM {uc_cart_products} cp
    LEFT JOIN {users} u ON u.uid = cp.cart_id
    LEFT JOIN {uc_abandoned_notifications} an ON an.primary_email = u.mail
    WHERE CHAR_LENGTH(cp.cart_id) < 22 AND cp.changed > :cart_cutoff_low AND cp.changed <= :cart_cutoff_high
    GROUP BY cp.cart_id HAVING COUNT(an.foreign_id) = 0 OR MAX(an.notified) < MAX(cp.changed)
    ORDER BY cp.cart_id DESC", array(':cart_cutoff_low' => $cart_cutoff_low, ':cart_cutoff_high' => $cart_cutoff_high))->fetchCol();

  return $cart_ids;
}
