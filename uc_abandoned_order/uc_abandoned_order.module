<?php
/**
 * @file
 * Periodically checks for abandoned orders and emails reminds to users with
 * links to complete their checkout.
 */

/**
 * Implements hook_cron().
 */
function uc_abandoned_order_cron() {
  // First notification; set timestamp for next reminder if necessary
  $order_ids = uc_abandoned_order_get_orders();
  $orders = uc_order_load_multiple($order_ids);
  foreach ($orders as $order) {
    if ($order->uid > 0 && $account = user_load($order->uid)) {
      if (!uc_abandoned_can_send_mail($account)) {
        continue;
      }
      db_insert('uc_abandoned_notifications')
        ->fields(array(
          'primary_email' => $order->primary_email,
          'notified' => REQUEST_TIME,
          'next_notification' => NULL,
          'type' => 'order',
          'foreign_id' => $order->order_id,
      ))->execute();
      rules_invoke_component('uc_abandoned_email_notification', $account->mail, uc_abandoned_get_products($order->products));
    }
  }
}

/**
 * Retrieves a list of users who have abandoned orders and who have not yet
 * received a notification.
 */
function uc_abandoned_order_get_orders() {
  // How far back in orders we'll look
  $order_cutoff_low = REQUEST_TIME - variable_get('uc_abandoned_order_reachback', 172800);

  // For each unique email, find the largest order ID of an abandoned order and
  // then search again for the largest order ID of any status and exclude the
  // result if we have a more recent, non-abandoned order on file for that
  // email. Then join against notification history and ensure we haven't sent
  // anything for that user yet.
  $order_ids = db_query("SELECT MAX(aband_o.order_id) FROM {uc_orders} aband_o
    LEFT JOIN {uc_orders} recent_o ON aband_o.primary_email = recent_o.primary_email
    LEFT JOIN {uc_abandoned_notifications} an ON an.primary_email = aband_o.primary_email
    WHERE aband_o.order_status = 'abandoned' AND aband_o.primary_email <> '' AND aband_o.modified > :order_cutoff_low
    GROUP BY aband_o.primary_email HAVING MAX(aband_o.order_id) = MAX(recent_o.order_id) AND (MAX(aband_o.modified) > MAX(an.notified) OR COUNT(an.foreign_id) = 0)
    ORDER BY aband_o.order_id DESC", array(':order_cutoff_low' => $order_cutoff_low))->fetchCol();

  return $order_ids;
}
